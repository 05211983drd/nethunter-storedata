NetHunter Store is an installable catalogue of free security applications for the Android platform.
The client makes it easy to browse, install, and keep track of updates on your device.

Android itself is open in the sense that you are free to install apks from anywhere you wish, but there are many good reasons for using the NetHunter Store as your free software app manager:

* Dedicated to the needs of security professionals
* Be notified when updates are available
* optionally download and install updates automatically
* Keep track of older and beta versions
* Filter apps that aren't compatible with the device
* Find apps via categories and searchable descriptions
* Access associated urls for donations, source code etc.
* Stay safe by checking repo index signatures and apk hashes

This app is built and signed by Kali NetHunter.
 
